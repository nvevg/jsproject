setTimeout(() => {
  let tasks = document.querySelectorAll('section.tasks-section div.unit-task-block');

  let res = [];

  for (const task of tasks) {
    let arr = task.querySelectorAll(':scope > div');

    let obj = {};
    obj.title = arr[0].textContent.trim();
    obj.content = arr[1].textContent.trim();

    res.push(obj);
  }

  let comments = ``;

  for (const item of res) {
    item.content = item.content.replace(/ +/ig, ' ').replaceAll('\n', ' ');

    let string = `
// ${item.title}
// ${item.content}
// ===================== \n`;

    comments += string;

  }
  navigator.clipboard.writeText(comments).then(function () {
    console.log('Async: Copying to clipboard was successful!');
  }, function (err) {
    console.error('Async: Could not copy text: ', err);
  });


}, 2000);









// prevent not numbers in input 
// let input = document.querySelector('input');
// input.addEventListener('keypress', function (e) {
//   if (isNaN(+e.key)) {
//     e.preventDefault();
//     return false;
//   }
// });


