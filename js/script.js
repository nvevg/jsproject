function askPassword(ok, fail) {
  let password = prompt("Password?", '');
  if (password == "rockstar") ok();
  else fail();
}

let user = {
  name: 'John',

  login(result) {
    console.log(this.name + (result ? ' logged in' : ' failed to log in'));
  }
};

function sayHi() {
  console.log('Привет');
}

function sayBye() {
  console.log('Bye');
}

// askPassword(() => user.login.call(user, true),() => user.login.call(user, false));

sayHi();
sayBye();
console.log("Добрый вечер!");

let b = 1;
let a = 2; // A Comment
console.log(a + b);