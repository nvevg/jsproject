// function explainEventLoop() {
//   console.log(1, 'Выполнится первым');
//   console.log(2, "Выполнится вторым");

//   setTimeout(() => {
//     console.log(8, "Якобы выполнится третьим");
//   }, 2000);

//   console.log(3, "Якобы выполнится четвертым");

//   setTimeout(() => {
//     console.log(5, "Якобы выполнится пятым");
//     setTimeout(() => {
//       console.log(6, "Якоб выполнится шестым");
//       doSomethingElse();
//     }, 0);
//   }, 1000);

//   console.log(4, "Якобы выполнится последним");
// }

// function doSomethingElse() {
//   console.log(7, "Делаем еще кое-что");
// }

// explainEventLoop()



// function explainEventLoop() {
//   console.log(1, 'Выполнится первым');
//   console.log(2, "Выполнится вторым");

//   setTimeout(() => {
//     console.log(5, "Якобы выполнится третьим");
//   }, 0);

//   console.log(3, "Якобы выполнится четвертым");

//   setTimeout(() => {
//     console.log(6, "Якобы выполнится пятым");
//     setTimeout(() => {
//       console.log(7, "Якоб выполнится шестым");
//       doSomethingElse();
//     }, 0);
//   }, 0);

//   console.log(4, "Якобы выполнится последним");
// }

// function doSomethingElse() {
//   console.log(8, "Делаем еще кое-что");
// }

// explainEventLoop()


// debounce function

// function debounce(callback, delay) {
//   let timer;

//   function fn(...args) {
//     clearTimeout(timer);
//     timer = setTimeout(() => {
//       callback.apply(this, args);
//     }, delay);
//   }
//   return fn;
// }